from selenium import webdriver
import unittest

from Base_Pages.CreditPages import CreditPages
from Base_Pages.RegistroPage import RegistroPage
from Base_Pages.IniciarSesionPage import IniciarSesionPage


class CreditoLineru(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path="C:/Users/ADMIN/PycharmProjects/AutomatizacionLineru/Drivers/chromedriver.exe")
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    #Test_1
    def test_Credito(self):
        # calcular credito
        self.driver.get("https://www.lineru.com/")
        driver = self.driver
        Credito = CreditPages(driver)
        Credito.Ingresar_Monto("500000")
        driver.save_screenshot("C:/Users/ADMIN/PycharmProjects/AutomatizacionLineru/Screenshots/Monto.png")
        Credito.IngresarFecha()
        driver.save_screenshot("C:/Users/ADMIN/PycharmProjects/AutomatizacionLineru/Screenshots/Fecha.png")
        Credito.SolicitarCredit()

    #Test_2
    def test_EmpezarRegistro(self):
        # Registro

        driver = self.driver
        Registro = RegistroPage(driver)
        Registro.Ingresar_Primer_nombre("Liliana")
        Registro.Tipo_documento()
        Registro.Num_documen("1236568")
        Registro.Num_Celular("7006247856")
        Registro.Ingrear_Correo("Test_Pruebas_@gmail.com")
        Registro.Ingresar_Contrasena("Test12345*")
        Registro.Terminos_Condiciones()
        Registro.Tratamiento_de_datos()
        #Captura de pantalla
        driver.save_screenshot("C:/Users/ADMIN/PycharmProjects/AutomatizacionLineru/Screenshots/Registro.png")
        Registro.Registrarse()
        driver.save_screenshot("C:/Users/ADMIN/PycharmProjects/AutomatizacionLineru/Screenshots/Sesion.png")
        Registro.BTN_Perfil()
        Registro.Cerrar_sesion()

    #Test_3
    def test_InicioSesion(self):
        # Inicio de sesion

        driver = self.driver
        Inicio = IniciarSesionPage(driver)
        Inicio.IncBoton()
        Inicio.Iniciar_Documento("1236565")
        Inicio.Iniciar_Contrasena("Test12345*")
        driver.save_screenshot("C:/Users/ADMIN/PycharmProjects/AutomatizacionLineru/Screenshots/Iniciarsesion.png")
        Inicio.Iniciar_sesion()


    @classmethod
    def tearDowmClass(cls):
        cls.driver.quit()
        cls.driver.close()

