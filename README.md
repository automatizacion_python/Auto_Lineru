**Lenguaje utilizado Python mediante Pycharm**

Se encuentra esructurado bajo el modelo Page Object de la siguiente manera:

1. Base_Pages: Se encuentran los metodos constriudos para dicha automatizacion
    a.CreditPages.py , Solicitud de credito
    b.RegistroPage.py , Registro en plataforma
    c.IniciarSesionPage.py ,  Inicio de sesion
2.Drivers: Ejecutable ChromeDriver.exe
3.Locators: Localizadores de Object (Xpath,id,etc)
4.Screenshots: Capturas de los test realizados
5.Test: Ejecutor de pruebas automatizadas

- Todos los test seran ejecutados desde la carpeta 5.Test
- Los 3 test estan hechos para que se ejecuten secuencialmente sin interrupciones
