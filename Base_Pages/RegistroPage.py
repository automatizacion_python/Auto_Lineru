from Locators.Locators import Locator

from selenium.webdriver import ActionChains
import time
#Registro en Lineru
class RegistroPage():

    def __init__(self, driver):
        self.driver = driver

        self.Cerrar_sesion_xpath = "/html/body/app-root/app-account/app-header-account/nav/div[2]/ul/li/div[2]/div[2]/a/span"

    def Ingresar_Primer_nombre(self, PNombre):
        self.driver.find_element_by_id(Locator.Primer_Nombre_id).send_keys(PNombre)
        time.sleep(2)

    def Tipo_documento(self):
        self.driver.find_element_by_xpath(Locator.Tipo_Doc_xpath).click()
        time.sleep(1)

        self.driver.find_element_by_id(Locator.Selec_Doc_id).click()
        time.sleep(1)

    def Num_documen(self, Docu):
        self.driver.find_element_by_id(Locator.Num_Doc_id).send_keys(Docu)
        time.sleep(1)

    def Num_Celular(self, Celular):
        self.driver.find_element_by_id(Locator.Num_Cel_id).send_keys(Celular)
        time.sleep(1)

    def Ingrear_Correo(self, Correo):
        self.driver.find_element_by_id(Locator.Correo_id).send_keys(Correo)
        time.sleep(1)

    def Ingresar_Contrasena(self, Contrasena):
        self.driver.find_element_by_id(Locator.Contrasena_id).send_keys(Contrasena)
        time.sleep(1)

    def Terminos_Condiciones(self):
        self.checkBox = self.driver.find_element_by_id(Locator.CheckTerminos_id)
        ActionChains(self.driver).move_to_element(self.checkBox).click(self.checkBox).perform()

    def Tratamiento_de_datos(self):
        self.checkBox2 = self.driver.find_element_by_id(Locator.CheckTratamiento_datos_id)
        ActionChains(self.driver).move_to_element(self.checkBox2).click(self.checkBox2).perform()
        time.sleep(1)

    def Registrarse(self):
        self.driver.find_element_by_xpath(Locator.Empieza_xpath).click()
        time.sleep(15)

    def BTN_Perfil(self):
        self.driver.find_element_by_id(Locator.Miperfil_id).click()
        time.sleep(2)

    def Cerrar_sesion(self):
        self.driver.find_element_by_xpath(Locator.Cerrar_sesion_xpath).click()
        time.sleep(5)

