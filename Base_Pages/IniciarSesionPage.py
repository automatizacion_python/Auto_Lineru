from Locators.Locators import Locator
import time

class IniciarSesionPage():

    def __init__(self, driver):
        self.driver = driver

    #Ir a "Mi cuenta"
    def IncBoton(self):
        self.driver.find_element_by_link_text(Locator.Iniciar_sesion_LinkText).click()
        time.sleep(1)
    #Ingresa Num Documento
    def Iniciar_Documento(self, DocIniciar):
        self.driver.find_element_by_id(Locator.NumDoc_id).click()
        self.driver.find_element_by_id(Locator.NumDoc_id).send_keys(DocIniciar)
        time.sleep(1)

    #Ingresa Contraseña
    def Iniciar_Contrasena(self, ContrasenaIniciar):
        self.driver.find_element_by_id(Locator.Contrasena_Inic_id).click()
        self.driver.find_element_by_id(Locator.Contrasena_Inic_id).send_keys(ContrasenaIniciar)
        time.sleep(1)
    #Iniciar sesion
    def Iniciar_sesion(self):
        self.driver.find_element_by_xpath(Locator.Btn_Iniciar_sesion_Xpath).click()
        time.sleep(10)