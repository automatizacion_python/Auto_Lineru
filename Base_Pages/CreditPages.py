from Locators.Locators import Locator
import time


#Solicitud de credito Lineru

class CreditPages():

    def __init__(self, driver):
        self.driver = driver
        #Monto que se desea solicitar
    def Ingresar_Monto(self, Monto):
        self.driver.find_element_by_id(Locator.BoxMonto_id).clear()
        self.driver.find_element_by_id(Locator.BoxMonto_id).send_keys(Monto)


        #Fecha de Pago
    def IngresarFecha(self):
        self.driver.find_element_by_id(Locator.FechPago_id).click()
        time.sleep(2)
        self.driver.find_element_by_xpath(Locator.FechaSelec_xpath).click()
        time.sleep(1)

        #Solicitud finalizada
    def SolicitarCredit(self):
        self.driver.find_element_by_xpath(Locator.BTNSolic_Credit_xpath).click()
        time.sleep(3)

