class Locator():

    #Localizadores de objetos

    #CreditPages
    BoxMonto_id = "mat-input-0"
    FechPago_id = "mat-input-1"
    FechaSelec_xpath = "//div[contains(text(),'17')]"
    BTNSolic_Credit_xpath = "/html/body/app-root/app-init/div[1]/div[2]/div[3]/shared-calculator/div/div[1]/div/form/button[1]"

    #RegistroPage
    Primer_Nombre_id = "mat-input-2"
    Tipo_Doc_xpath = "/html/body/app-root/app-auth-register/div/div[2]/div/div/div[1]/div/form/div[1]/div[1]/app-select/mat-form-field/div/div[1]/div/mat-select/div/div[2]"
    Selec_Doc_id = "mat-option-1"
    Num_Doc_id = "mat-input-3"
    Num_Cel_id = "mat-input-4"
    Correo_id = "mat-input-5"
    Contrasena_id = "mat-input-6"
    CheckTerminos_id = "terms-0"
    CheckTratamiento_datos_id = "copyright-0"
    Empieza_xpath = "/html/body/app-root/app-auth-register/div/div[2]/div/div/div[1]/div/form/div[3]/button"
    Mensaje_xpath = "/html/body/app-root/app-account/app-header-account/nav/div[2]/ul/li/div[1]/div/div/div[1]/span[1]"
    Miperfil_id = "navbarDropdown"
    Cerrar_sesion_xpath = "/html/body/app-root/app-account/app-header-account/nav/div[2]/ul/li/div[2]/div[2]/a/span"

    #IniciarSesionPage
    Iniciar_sesion_LinkText = "MI CUENTA"
    Btn_Iniciar_sesion_Xpath = "/html/body/app-root/app-auth-login/div/div[2]/div/div/div[1]/div/form/div[1]/button"
    NumDoc_id = "mat-input-2"
    Contrasena_Inic_id = "mat-input-3"
